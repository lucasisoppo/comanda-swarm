package com.lucasisoppo.comanda.rest.resources;

import com.lucasisoppo.comanda.model.Mesa;
import com.lucasisoppo.comanda.model.Pedido;
import com.lucasisoppo.comanda.rest.AbstractCrudResource;
import com.lucasisoppo.comanda.services.AbstractCrudService;
import com.lucasisoppo.comanda.services.MesaService;
import com.lucasisoppo.comanda.services.PedidoService;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

@Path("mesas")
public class MesaResource extends AbstractCrudResource<Mesa>{
    @Inject
    private MesaService service;
    
    @Override
    protected AbstractCrudService<Mesa> getService() {
        return service;
    }
    
    @Inject
    private PedidoService servicePedido;
    
    @GET
    @Path("pedido-em-aberto")
    @Produces(MediaType.APPLICATION_JSON)
    public Pedido getPedidoEmAberto(@QueryParam("mesa") Long idMesa) {
        return servicePedido.findByPedidoEmAberto(idMesa);
    }
    
    
}
