package com.lucasisoppo.comanda.rest.resources;

import com.lucasisoppo.comanda.model.Pedido;
import com.lucasisoppo.comanda.rest.AbstractCrudResource;
import com.lucasisoppo.comanda.services.AbstractCrudService;
import com.lucasisoppo.comanda.services.PedidoService;
import javax.inject.Inject;
import javax.ws.rs.Path;

@Path("pedidos")
public class PedidoResource extends AbstractCrudResource<Pedido> {

    @Inject
    private PedidoService service;
    
    @Override
    protected AbstractCrudService<Pedido> getService() {
        return service;
    }
    
}
