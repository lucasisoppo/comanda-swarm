package com.lucasisoppo.comanda.rest.resources;

import com.lucasisoppo.comanda.model.Produto;
import com.lucasisoppo.comanda.rest.AbstractCrudResource;
import com.lucasisoppo.comanda.services.AbstractCrudService;
import com.lucasisoppo.comanda.services.ProdutoService;
import javax.inject.Inject;
import javax.ws.rs.Path;

@Path("produtos")
public class ProdutoResource extends AbstractCrudResource<Produto> {

    @Inject
    private ProdutoService service;
    
    @Override
    protected AbstractCrudService<Produto> getService() {
        return service;
    }
    
}
