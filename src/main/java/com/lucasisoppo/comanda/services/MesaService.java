package com.lucasisoppo.comanda.services;

import com.lucasisoppo.comanda.model.Mesa;
import com.lucasisoppo.comanda.utils.GenericDao;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;

@Stateless
@TransactionAttribute(TransactionAttributeType.SUPPORTS)
public class MesaService extends AbstractCrudService<Mesa>{

    @Inject
    private GenericDao<Mesa> dao;
    
    @Override
    protected GenericDao<Mesa> getDao() {
       return dao;
    }
    
    
}
