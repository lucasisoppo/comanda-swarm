package com.lucasisoppo.comanda.services;

import com.lucasisoppo.comanda.model.Mesa;
import com.lucasisoppo.comanda.model.Pedido;
import com.lucasisoppo.comanda.rest.resources.PedidoResource;
import com.lucasisoppo.comanda.utils.GenericDao;
import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

@Stateless
@TransactionAttribute(TransactionAttributeType.SUPPORTS)
public class PedidoService extends AbstractCrudService<Pedido> {

    @PersistenceContext
    private EntityManager em;

    @Inject
    private GenericDao<Pedido> dao;

    @Override
    protected GenericDao<Pedido> getDao() {
        return dao;
    }

    public Pedido findByPedidoEmAberto(Long idMesa) {
        TypedQuery<Pedido> query = em.createQuery("SELECT p FROM Pedido p where p.pagamento is null and p.mesa.id = :mesa ", Pedido.class);
        query.setParameter("mesa", idMesa);
        return (Pedido) query.getSingleResult();
    }
}
