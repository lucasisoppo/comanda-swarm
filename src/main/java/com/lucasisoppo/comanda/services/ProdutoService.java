package com.lucasisoppo.comanda.services;

import com.lucasisoppo.comanda.model.Produto;
import com.lucasisoppo.comanda.utils.GenericDao;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;

@Stateless
@TransactionAttribute(TransactionAttributeType.SUPPORTS)
public class ProdutoService extends AbstractCrudService<Produto>{

    @Inject
    private GenericDao<Produto> dao;
    
    @Override
    protected GenericDao<Produto> getDao() {
        return dao;
    }
}
