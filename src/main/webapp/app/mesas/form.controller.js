(function () {
    'use strict'

    angular.module('app')
        .controller('MesaFormController', MesaFormController);

    MesaFormController.$inject = ['MesaService', '$state', '$stateParams', 'DialogBuilder', 'PedidoService'];

    function MesaFormController(MesaService, $state, $stateParams, DialogBuilder) {

        var vm = this;
        vm.registro = {
          //  pedidos: []
        };
        vm.error = {};
        vm.titulo = 'Nova mesa';

        vm.salvar = salvar;

        if ($stateParams.id) {
            MesaService.findById($stateParams.id)
                .then(function (data) {
                    vm.registro = data;
                //    vm.registro.pedidos = vm.registro.pedidos || [];
                    vm.titulo = 'Editando Mesa';
                });
        }

        function salvar() {
            if (!vm.registro.id) {
                MesaService.insert(vm.registro)
                    .then(function (dado) {
                        DialogBuilder.message('Registro inserido com sucesso!');
                        $state.go("mesasList");
                    })
                    .catch(function (error) {
                        vm.error = error.data;
                    });
            } else {
                MesaService.update(vm.registro)
                    .then(function (dado) {
                        DialogBuilder.message('Registro alterado com sucesso!');
                        $state.go("mesasList");
                    })
                    .catch(function (error) {
                        vm.error = error.data;
                    });
            }
        }
    }
})();