(function () {
    'use strict'

    angular.module('app')
        .controller('MesaListController', MesaListController);

    MesaListController.$inject = [
        'MesaService',
        '$state'
    ];

    function MesaListController(MesaService, $state) {
        var vm = this;
        vm.data = {}; 
        

        vm.atualizar = load;
        vm.resetFiltro = function () {
            vm.filtro = '';
            load();
        }

        function load() {
            MesaService.findAll()
                .then(function (dados) {
                    vm.data = dados
                });
        }
        
        vm.adicionar = function (mesa) {
            vm.idPedido ='';
            MesaService.findPedidoEmAberto(mesa).
            then(function (dado) {
                vm.idPedido = dado;
               // alert('Caiu ID'+ vm.idPedido);
                $state.go('pedidosEditar',{id: vm.idPedido});
            })
            .catch(function () {
               // alert('Caiu Sem ID'+ vm.idPedido);
                MesaService.idMesa = mesa;
                //alert('Mesa'+ MesaService.idMesa);
                $state.go('pedidosNovo'); 
              
            });       
        };
        load();
    }
})();