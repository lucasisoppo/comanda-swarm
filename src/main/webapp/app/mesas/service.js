(function () {
    'use strict'

    angular.module('app')
        .service('MesaService', MesaService);

    MesaService.$inject = ['$http'];

    function MesaService($http) {
        var idMesa;

        function findPedidoEmAberto(idMesa) {
            return $http.get('http://localhost:8080/api/mesas/pedido-em-aberto?mesa=' + idMesa)
                .then(function (response) {
                    return response.data.id;
                });
        }

        function findById(id) {
            return $http.get('http://localhost:8080/api/mesas/' + id)
                .then(function (response) {
                    return response.data;
                });
        }
        function findAllOver() {
            return $http.get('http://localhost:8080/api/mesas/all?order=numero')
                .then(function (response) {
                    return response.data;
                });
        }
        function findAll() {
            return $http.get('http://localhost:8080/api/mesas')
                .then(function (response) {
                    return {registros: response.data
                        }   
                });
        }

        function insert(registro) {
            return $http.post('http://localhost:8080/api/mesas', registro)
                .then(function (response) {
                    return response.data;
                });
        }

        function update(registro) {
            return $http.put('http://localhost:8080/api/mesas/' + registro.id, registro)
                .then(function (response) {
                    return response.data;
                });
        }

        function remove(id) {
            return $http.delete('http://localhost:8080/api/mesas/' + id)
                .then(function (response) {
                    return response.data;
                });
        }

        return {
            idMesa:idMesa,
            findPedidoEmAberto:findPedidoEmAberto,
            findAllOver: findAllOver,
            findById:findById,
            findAll: findAll,
            insert: insert,
            update: update,
            remove: remove
        }
    }

})();