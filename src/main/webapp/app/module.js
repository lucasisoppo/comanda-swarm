(function () {

    angular.module('app', [
        'ui.router'
    ]);

    angular.module('app').config(AppConfig);

    AppConfig.$inject = ['$stateProvider'];

    function AppConfig($stateProvider) {
        $stateProvider
            .state({
                name: 'mesasList',
                url: '/mesas',
                templateUrl: '/views/mesas/list.html',
                controller: 'MesaListController',
                controllerAs: 'vm'
            })
            .state({
                name: 'mesasEditar',
                url: '/mesas/{id}',
                templateUrl: '/views/mesas/form.html',
                controller: 'MesaFormController',
                controllerAs: 'vm'
            })
            .state({
                name: 'mesasNovo',
                url: '/mesas/novo',
                templateUrl: '/views/mesas/form.html',
                controller: 'MesaFormController',
                controllerAs: 'vm'
            })
            .state({
                name: 'clientesList',
                url: '/clientes',
                templateUrl: '/views/clientes/list.html',
                controller: 'ClienteListController',
                controllerAs: 'vm'
            })
            .state({
                name: 'produtosList',
                url: '/produtos',
                templateUrl: '/views/produtos/list.html',
                controller: 'ProdutoListController',
                controllerAs: 'vm'
            })
            .state({
                name: 'produtosNovo',
                url: '/produtos/novo',
                templateUrl: '/views/produtos/form.html',
                controller: 'ProdutoFormController',
                controllerAs: 'vm'
            })
            .state({
                name: 'produtosEditar',
                url: '/produtos/{id}',
                templateUrl: '/views/produtos/form.html',
                controller: 'ProdutoFormController',
                controllerAs: 'vm'
            })
            .state({
                name: 'pedidosList',
                url: '/pedidos',
                templateUrl: '/views/pedidos/list.html',
                controller: 'PedidoListController',
                controllerAs: 'vm'
            })
            .state({
                name: 'pedidosNovo',
                url: '/pedidos/novo',
                templateUrl: '/views/pedidos/form.html',
                controller: 'PedidoFormController',
                controllerAs: 'vm'
            })
            .state({
                name: 'pedidosEditar',
                url: '/pedidos/{id}',
                templateUrl: '/views/pedidos/form.html',
                controller: 'PedidoFormController',
                controllerAs: 'vm'
            })
;
    }
})();